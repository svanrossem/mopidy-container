FROM fedora:35
MAINTAINER Sander van Rossem <s.vanrossem@gmail.com>

# Variables
ENV PYTHON_VERSION=3.8
ENV MOPIDY_INSTALL_DIR=/opt/mopidy

# Set dnf environment
RUN echo "install_weak_deps=False" >> /etc/dnf/dnf.conf
RUN echo "fastestmirror=1" >> /etc/dnf/dnf.conf
RUN echo "tsflags=nodocs" >> /etc/dnf/dnf.conf

# Create mopidy user
RUN useradd --user-group --system --create-home --no-log-init mopidy

# Install all libraries and needs
# https://src.fedoraproject.org/rpms/mopidy/blob/rawhide/f/mopidy.spec
RUN dnf upgrade --assumeyes
RUN dnf install --assumeyes \
                gcc \
                tar \
                pulseaudio \
                gstreamer1-plugins-base gstreamer1-plugins-good gstreamer1-plugins-bad-free \
                rust-gstreamer+serde-devel \
                python${PYTHON_VERSION} \
                gobject-introspection-devel \
                cairo-gobject-devel

# Install gstreamer-spotify (EXPERIMENTAL)
WORKDIR /build
RUN dnf install --assumeyes git cargo gstreamer1-devel
RUN git clone --depth 1 -b main https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs.git \
 && cd gst-plugins-rs \
 && git branch \
 && sed -i 's/librespot = { version = "0.4", default-features = false }/librespot = { version = "0.4.2", default-features = false }/g' audio/spotify/Cargo.toml \
 && cargo build --no-default-features -p gst-plugin-spotify -r \
 && cp ./target/release/libgstspotify.so /usr/lib64/gstreamer-1.0/ \
 && ln -s /usr/lib64/gstreamer-1.0/libgstspotify.so /usr/lib/libgstspotify.so \
 && cd .. \
 && rm -rf gst-plugins-rs \
 && dnf remove --assumeyes cargo gstreamer1-devel

# Setup Python virtual environment
RUN python${PYTHON_VERSION} -m venv ${MOPIDY_INSTALL_DIR}
ENV PATH="${MOPIDY_INSTALL_DIR}/bin:${PATH}"

# Install Python package manager pip
# https://pypi.org/project/pip/
RUN python${PYTHON_VERSION} -m ensurepip --upgrade
RUN python${PYTHON_VERSION} -m pip install --upgrade pip

# Install PyGObject
# https://pypi.org/project/PyGObject/
RUN python${PYTHON_VERSION} -m pip install pygobject

# Install cffi from source
#  note: libffi-devel for Fedora is too old, hardcoded stuff
# https://pypi.org/project/cffi/
ENV CFFI_VERSION=1.15.0
RUN curl -so cffi-${CFFI_VERSION}.tar.gz https://files.pythonhosted.org/packages/00/9e/92de7e1217ccc3d5f352ba21e52398372525765b2e0c4530e6eb2ba9282a/cffi-${CFFI_VERSION}.tar.gz \
 && tar -xzf cffi-${CFFI_VERSION}.tar.gz --strip-components=1 \
 && python${PYTHON_VERSION} setup.py install \
 && rm -rf *

# Install Mopidy
WORKDIR /opt/mopidy
COPY requirements.txt .
RUN python${PYTHON_VERSION} -m pip install -r requirements.txt

# install mopidy-spotify-gstspotify
RUN git clone -b main https://gitlab.com/svanrossem/mopidy-spotify-gstspotify.git mopidy-spotify \
 && cd mopidy-spotify \
 && python3 setup.py install \
 && cd .. \
 && rm -rf mopidy-spotify \
 && dnf remove --assumeyes git

# Patch Iris
## disable service worker (cache)
RUN rm -f /opt/mopidy/lib/python3.8/site-packages/mopidy_iris/static/service-worker.js
## https://github.com/jaedb/Iris/blob/master/mopidy_iris/system.sh#L3-L7
RUN echo "1" >> /IS_CONTAINER
## disable update check
RUN sed -i 's/upgrade_available = upgrade_available == 1/upgrade_available = False/g' ${MOPIDY_INSTALL_DIR}/lib/python${PYTHON_VERSION}/site-packages/mopidy_iris/core.py
## .button--destructive: restart server and reset settings button
## .flag--dark: uptodate label
## .sub-tabs--servers: server configuration
RUN sed -i 's/<style>/<style> .progress .slider {cursor: not-allowed !important;} .progress .slider__input {pointer-events: none !important;} .button--destructive {display: none !important} .flag--dark {display: none !important} .sub-tabs--servers {display: none !important}/g' ${MOPIDY_INSTALL_DIR}/lib/python${PYTHON_VERSION}/site-packages/mopidy_iris/static/index.html
## patch system.sh with PATH and disable hardcoded _USE_SUDO!
RUN sed -i "2i export PATH=${PATH}" ${MOPIDY_INSTALL_DIR}/lib/python${PYTHON_VERSION}/site-packages/mopidy_iris/system.sh \
 &&  sed -i 's/_USE_SUDO = True/_USE_SUDO = False/g' ${MOPIDY_INSTALL_DIR}/lib/python${PYTHON_VERSION}/site-packages/mopidy_iris/system.py

# Cleanup
RUN dnf clean all && rm -rf /var/cache/dnf/* && rm -rf /root/.cache

# Leave root and become mopidy, rootlesssss magics
USER mopidy

RUN mkdir -p /home/mopidy/.config \
 && ln -s /config /home/mopidy/.config/mopidy

WORKDIR /home/mopidy

EXPOSE 6600 6680

CMD ["mopidy"]
