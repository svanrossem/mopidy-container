# mopidy-container
####
Iris is an extension for the Mopidy music server. With support for Spotify, LastFM, Genius, Snapcast and many other extensions, Iris brings all your music into one user-friendly and unified web-based interface that works beautifully, no matter your device.
